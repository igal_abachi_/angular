
# to run preact test

npm install -g preact-cli

npm install

preact serve

# to run ng2 client web site:(http://localhost:4200/Issues)

install python 2.7

install node js 8.7 64bit current

npm install -g typescript@2.0.3

(make sure no other version of angular installed, before executing this command)

npm install -g angular-cli@1.0.0-beta.11-webpack.8

npm install (inside project folder)

ng serve


# compiled version
instead of installing , there is compiled version in /dist folder
also nginx folder that is configured to serve /dist folder
use /nginx/start nginx.cmd




# to run web api server: (http://localhost:3000/api/controller)
download & install .NET Core 2.0 SDK (maybe runtime is enough , and no need for full sdk)

https://www.microsoft.com/net/download/core

then run compiled dll in /bin folder:

dotnet issueServer.dll

server will be soon , served from the internet:
you can call http://localhost:3000/api/reset
to reset the data , when it will be served from the internet


