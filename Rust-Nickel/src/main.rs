#[macro_use]
extern crate nickel;
use nickel::{HttpRouter, Nickel, MediaType};

mod main_controller;

fn main() {
	let mut server = Nickel::new();

	server.get(
		"/users/:id",
		middleware! { |r,mut response|
			let id = r.param("id").unwrap();
			"User: ".to_string() + id
			
			//let _res =main_controller::users(id);
		
			//response.set(MediaType::Json);
			//_res
		}
	);

	let _res = server.listen("127.0.0.1:3000");
}
