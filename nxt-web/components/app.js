import { h, Component } from 'preact';
import { Router } from 'preact-router';

import Header from './header';
import Footer from './footer';
import Home from '../routes/home';

// import Home from 'async!../routes/home';
// import Profile from 'async!../routes/profile';

export default class App extends Component {
    /** Gets fired when the route changes.
     *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
     *	@param {string} event.url	The newly routed URL
     */
    handleRoute = e => {
        this.currentUrl = e.url;
    };

    render() {
        return (
			<div id="app" class="container" style="height:100%;max-width: 1680px;">
				<div class="row" style="height:100%">
					<div class="col-md-12" style="height:100%">
						<div style="min-height:100vh;height: 100%;" id="app-root">
							<div class="row">
								<div class="col-md-10 offset-md-1 content-bg">
									<Header />
									<Router onChange={this.handleRoute}>
										<Home path="/" />
									</Router>
									<Footer />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        );
    }
}
