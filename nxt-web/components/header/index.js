import {h, Component} from 'preact';
import {Link} from 'preact-router/match';
import style from './style';

export default () => (
    <div class="row">
        <div class="col-md-12">

            <nav class="navbar navbar-expand navbar-light bg-light">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Nxt-Web <img src="../../assets/mstile-310x310.png"
                                                                  style="height:30px;margin-right:10px;"></img></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#project-types">סוגי אתרים</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#services">שירותי פיתוח אתרים</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#custom-app">פיתוח אתר מותאם אישית</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about">קצת עליי</a>
                    </li>

                    <li class="nav-item hidden-lg-down hideMobile" style="margin-right: 30px;">

                        <h1 style="font-size: 20pt;color:darkblue;background-color: rgba(0,255,0,0.1);"
                            align="center" class="hidden-lg-down hideMobile">התקשרו עכשיו! וקבלו הצעת מחיר: 054-290-5982 יגאל</h1>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
);
