import {h, Component} from 'preact';
import style from './style';
import Custom from "./custom";

export default ({}) => (
    <div class="row">

        <div class="col-md-12">
            <a name="custom-app"></a>
            <h1 style="font-size: 20pt;margin-top:10px;background-color: rgba(255,255,255,0.2);" align="center">פיתוח אתר/אפליקציה בהתאמה אישית:</h1>
        </div>

        <div class="col-md-12">
            <div class="row" style="margin-top:20px;">
                <div class="col-md-12">
                    <Custom/>
                </div>
            </div>
        </div>
    </div>
);

