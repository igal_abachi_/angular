import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-globe"></i> <span style="font-weight: bold;">אתר/אפליקציה בהתאמה אישית</span>
        </div>
        <div class="card-body">

            <p class="card-text">הכי הרבה גמישות לפיתוחים מורכבים מהרגיל , מערכת client-server , שירותי ענן
                שמבצעים כל מיני אלגורתמים...</p>
            <p class="card-text">שימוש בטוכנולוגיות מתקדמות:</p>

            <p class="card-text" dir="ltr">
                <img src="./../assets/img/ng5.png"/>Angular 5 cli,<img src="./../assets/img/vue.png"/> VueJS 2 ,<img
                src="./../assets/img/preact.png"/> Preact cli,<img src="./../assets/img/ts.png"/> typescript 2.7 ,<img
                src="./../assets/img/sass.png"/> sass/scss ,<img src="./../assets/img/bootstrap.png"/> bootstrap 4 ,<img
                src="./../assets/img/mongo.png"/> mongo db ,
                <img src="./../assets/img/aerospike.png"/> aerospike , <img src="./../assets/img/elk.png"/> Elastic Stack
                (Elasticsearch),<img src="./../assets/img/rabbit.png" style="margin-top: 3px;margin-bottom: 3px;"/>
                RabbitMQ ,<img src="./../assets/img/asp.png" style="margin-top: 3px;margin-bottom: 3px;"/> asp.net
                core 2(kestrel web api) REST
                web service, <img src="./../assets/img/cs.png" style="margin-top: 3px;margin-bottom: 3px;"/> c# .net
                core ,<img src="./../assets/img/node.png" style="margin-top: 3px;margin-bottom: 3px;"/> node.js 9,
                <img src="./../assets/img/restify.png" style="margin-top: 3px;margin-bottom: 3px;"/> restify, msgpack,
                MobX state managment, JavaScript
                ES6 , CSS3 , HTML5, JSON, <img src="./../assets/img/socket.png"/>Socket.IO, webpack , feathers.js /
                LoopBack 4 micro-services, git, D3
            </p>
        </div>
    </div>
);

