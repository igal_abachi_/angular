import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-eye"></i><span style="font-weight: bold;"> עיצוב UI / UX  </span>
        </div>
        <div class="card-body">
            <p class="card-text">
                עיצוב נעים, חווית משתמש נוחה, אתר נגיש, ברמת גימור גבוהה

            </p>
            <p class="card-text">
                אנימציות, עיצוב לוגו וכרטיסי ביקור
            </p>
            <p class="card-text">
                <img src="./../assets/img/ai.png"/> Photoshop CC <img src="./../assets/img/ps.png"/> Illustrator CC
            </p>
        </div>
    </div>
);

