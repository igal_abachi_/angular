import {h, Component} from 'preact';
import style from './style';

import Content from "./content";
import Hosting from "./hosting";
import MarketResearch from "./marketResearch";
import ResponsiveCompat from "./responsiveCompat";
import Security from "./security";
import Seo from "./seo";
import Specification from "./specification";
import UiUx from "./uiUx";

export default ({}) => (
    <div class="row">

        <div class="col-md-12">
            <a name="services"></a>
            <h1 style="font-size: 20pt;margin-top:10px;background-color: rgba(255,255,255,0.2);" align="center">שירותי
                פיתוח אתרים:</h1>
        </div>

        <div class="col-md-12">
            <div class="row" style="margin-top:20px;">


                <div class="col-md-4">
                    <MarketResearch/>
                </div>

                <div class="col-md-4">
                    <Specification/>
                </div>

                <div class="col-md-4">
                    <UiUx/>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="row" style="margin-top:20px;">


                <div class="col-md-4">
                    <Seo/>
                </div>

                <div class="col-md-4">
                    <ResponsiveCompat/>
                </div>

                <div class="col-md-4">
                    <Security/>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="row" style="margin-top:20px;">
                <div class="col-md-6">
                    <Hosting/>
                </div>
                <div class="col-md-6">
                    <Content/>
                </div>
            </div>
        </div>
    </div>
);

