import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-server"></i> <span style="font-weight: bold;">שיוך לDomain ואחסון אתר</span>
        </div>
        <div class="card-body" style="min-height: 250px;">

            <p class="card-text">רישום דומיין (שם מתחם): Triple C בארץ<img src="./../assets/img/ccc.png" style="margin-right: 5px;"/>, namecheap
                בחו"ל <img src="./../assets/img/namecheap.png"/></p>
            <p class="card-text">אחסון:<br/> LiquidWeb VPS (asp.net) , 1&1 hosting (shared) , WP engine (wordpress)</p>
            <p class="card-text" dir="ltr">
                <img src="./../assets/img/liquid-web.png" style="margin-right: 5px;"/>
                <img src="./../assets/img/1and1.png" style="margin-right: 5px;"/>
                <img src="./../assets/img/wp-engine.png"/></p>
        </div>
    </div>
);

