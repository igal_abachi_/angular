import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-search"></i> <span style="font-weight: bold;">SEO קידום ושיווק , (אורגני / ממומן)</span>
        </div>
        <div class="card-body" style="min-height: 250px;">
            <p class="card-text">שימוש ב: Yoast , SEMrush , google analytics</p>
            <p class="card-text">האתר שלך יופיע למעלה בתוצאות חיפוש ב:</p>
            <p class="card-text"> Google, Yahoo, Aol, Yandex, Bing</p>
            <p class="card-text" dir="ltr">
                <img src="./../assets/img/google.png"/>
                <img src="./../assets/img/yahoo.png" style="height: 22px;"/>
                <img src="./../assets/img/aol.png"/>
                <img src="./../assets/img/yandex.png"/>
            </p>
        </div>
    </div>
);

