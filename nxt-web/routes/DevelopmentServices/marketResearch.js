import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-user-secret"></i> <span style="font-weight: bold;">מחקר שוק ומיתוג נכון</span>
        </div>
        <div class="card-body">
            <p class="card-text">נדאג שהאתר שלך ייבלוט לעומת מתחרים</p>
            <p class="card-text">ותיהיה לעסק נוחכות ברשתות החברתיות</p>
            <p class="card-text" dir="ltr">
                <img src="./../assets/img/fb.png"/>
                <img src="./../assets/img/youtube.png"/>
            </p>
        </div>
    </div>
);

