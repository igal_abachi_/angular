import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-camera-retro"></i> <span style="font-weight: bold;">צילומי תוכן לאתר</span>
        </div>
        <div class="card-body" style="min-height: 250px;">

            <p class="card-text">צילום העסק, הבעלים והמוצרים, ווידאו ב 4K<img src="./../assets/img/4k.png"
                                                                              style="margin-right: 5px;"/>
            </p>
            <p class="card-text">באופן איכותי עם ציוד צילום מקצועי</p>
            <p class="card-text">התאמת התמונות לגלישה מהירה</p>
        </div>
    </div>
);

