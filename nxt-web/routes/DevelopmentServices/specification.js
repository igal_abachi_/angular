import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-edit"></i> <span style="font-weight: bold;">איפיון אתר</span>
        </div>
        <div class="card-body">
            <p class="card-text">איפיון אתר , ארכיטקטורת מידע ותכנון הניווט <img src="./../assets/img/docx.png"/></p>
            <p class="card-text" style="margin-top: 60px;"></p>
        </div>
    </div>
);

