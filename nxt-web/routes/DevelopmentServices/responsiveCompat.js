import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-tablet-alt"></i><span style="font-weight: bold;">אתרים ריספונסיביים ותואמים לכל הדפדפנים</span>
        </div>
        <div class="card-body" style="min-height: 250px;">
            <p class="card-text">נדאג שהאתר שלך ייפעל גם בפלאפונים וטאבלטים בנוסף למחשב</p>
            <p class="card-text" dir="ltr"> תאימות לדפדפני:<br/>
                <img src="./../assets/img/chrome.png"/>Chrome,
                <img src="./../assets/img/firefox.png"/>FireFox,
                <img src="./../assets/img/edge.png"/> Edge, Safari, Opera, Maxthon,
                Lunascape, Vivaldi,
                Brave</p>
            <p class="card-text"> בדיקה ושיפור ביצועים באתר לגלישה מהירה</p>
        </div>
    </div>
);

