import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-lock"></i><span style="font-weight: bold;">אתר מאובטח</span>
        </div>
        <div class="card-body" style="min-height: 250px;">
            <p class="card-text">
                הגנה ממתקפות: DDOS , noSQL injection , XSS , CSRF
                <br/>
                שימוש ב:  jwt ,oauth2 ,https, cloudflare
                <br/>
                <img src="./../assets/img/jwt.png"/>
                <img src="./../assets/img/cloudflare.png"/>
            </p>
        </div>
    </div>
);

