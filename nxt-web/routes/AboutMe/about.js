import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-user-circle"></i> <span style="font-weight: bold;">יגאל אבאצ'י - Igal Abachi</span>
        </div>
        <div class="card-body">

            <p class="card-text">
                <a href="https://www.linkedin.com/in/igalabachi/">
                    <img src="./../assets/img/linkedin.png"/> <span class="span-link" style="display: inline-block;"> פרופיל לינקדאין</span>
                </a>
            </p>
            <p class="card-text">
                <div class="row">
                    <div class="col-md-9">
                        עוסק בפיתוח 9 שנים, התחלתי מגיל קטן להתעסק במחשבים, הייתי ביחידה טכולוגית
                        בצבא, ומאז הייתי בכמה חברות בתחומים שונים, עובד בעיקר באזור המרכז<br/>
                        עסקתי בפיתוח Fullstack צד לקוח ושרת ,פיתוח Desktop וגם WEB ,תכנות בתחומים
                        של:
                        הגנה אווירית , סייבר , מודיעין, פיננסים ,ורפואה

                    </div>
                    <div class="col-md-3">
                        <img src="./../assets/img/igal_abachi.jpg" style="height: 170px;float: left;"/>
                    </div>
                </div>
            </p>

        </div>
    </div>
);

