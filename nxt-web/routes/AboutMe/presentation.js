import {h, Component} from 'preact';
import style from './style';
import About from "./about";

export default ({}) => (
    <div class="row">

        <div class="col-md-12">
            <a name="about"></a>
            <h1 style="font-size: 20pt;margin-top:10px;background-color: rgba(255,255,255,0.2);" align="center">קצת עליי:</h1>
        </div>

        <div class="col-md-12">
            <div class="row" style="margin-top:20px;">
                <div class="col-md-12">
                    <About/>
                </div>
            </div>
        </div>
    </div>
);

