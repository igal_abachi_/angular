import { h, Component } from 'preact';
import style from './style';

import ProjectTypes from "../ProjectTypes/index";
import DevelopmentServices from "../DevelopmentServices/index";
import CustomWebApp from "../CustomWebApp/index";
import AboutMe from "../AboutMe/index";


export default class Home extends Component {
    render() {
        return (
			<div class="row">
				<div class="col-md-12">
					<ProjectTypes/>
					<DevelopmentServices/>
					<CustomWebApp/>
					<AboutMe/>
				</div>
			</div>

        );
    }}
