import {h, Component} from 'preact';
import style from './style';

import Tadmit from "./tadmit";
import Store from "./store";
import Cms from "./cms";
import Blog from "./blog";

export default ({}) => (
    <div class="row">

        <div class="col-md-12">
            <a name="project-types"></a>

            <h1
                style="font-size: 20pt;margin-top:10px;background-color: rgba(255,255,255,0.2);"
                align="center">סוגי אתרים/פרוייקטים:</h1>
        </div>

        <div class="col-md-12">
            <div class="row" style="margin-top:20px;">
                <div class="col-md-6">
                    <Tadmit/>
                </div>
                <div class="col-md-6">
                    <Blog/>
                </div>
            </div>


        </div>


        <div class="col-md-12">
            <div class="row" style="margin-top:20px;">
                <div class="col-md-6">
                    <Cms/>
                </div>
                <div class="col-md-6">
                    <Store/>
                </div>
            </div>

        </div>
    </div>
);

