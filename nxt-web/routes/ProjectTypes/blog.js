import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-globe"></i> <span style="font-weight: bold;">בלוג ghost / WordPress</span>
        </div>
        <div class="card-body">

            <p class="card-text"> עם פורום myBB / XenForo</p>
            <p class="card-text">
                <img src="./../assets/img/wp.png"/>
                <img src="./../assets/img/ghost.png"/>
                <img src="./../assets/img/xenforo.png"/>
                <img src="./../assets/img/mybb.png"/>
            </p>
        </div>
    </div>
);

