import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-globe"></i> <span style="font-weight: bold;">אתר תוכן CMS מבוסס WordPress.org</span>
        </div>
        <div class="card-body">

            <p class="card-text">אתר מעוצב ויפה</p>
            <p class="card-text">כולל הדרכה איך לעבוד עם הממשק ניהול ולהוסיף תוכן משלך</p>
            <p class="card-text">
                <img src="../../assets/img/wp.png"/>
                <img src="./../assets/img/elegant.png"/>
                <img src="./../assets/img/themeforest.png"/>
                <img src="./../assets/img/studiopress.png"/>
            </p>
        </div>
    </div>
);

