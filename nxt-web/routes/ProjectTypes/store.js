import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-globe"></i> <span
            style="font-weight: bold;">חנות WooCommerce לאתר וורדפרס</span>
        </div>
        <div class="card-body">

            <p class="card-text">סליקה: טרנזילה , PayPal, CreditGuard</p>
            <p class="card-text">
                <img src="./../assets/img/woo.png"/>
                <img src="./../assets/img/paypal.png"/>
                <img src="./../assets/img/cg.png"/>
                <img src="./../assets/img/tranzila.png"/>
                <img src="./../assets/img/pci-dss.png"/>
                <img src="./../assets/img/ssl.png"/>
            </p>
            <p class="card-text">קניה בתקן PCI DSS , הצפנת Let’s Encrypt SSL</p>
        </div>
    </div>
);

