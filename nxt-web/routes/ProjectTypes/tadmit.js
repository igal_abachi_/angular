import {h, Component} from 'preact';
import style from './style';

export default ({}) => (
    <div class="card">
        <div class="card-header">
            <i class="fas fa-globe"></i> <span style="font-weight: bold;">אתר תדמית לעסקים , בכתיבה אישית או בWIX</span>
        </div>
        <div class="card-body">

            <p class="card-text">
                עם כפתורי שיתוף GetSocial ,
                ורשימות דיוור GetResponse ,
                צ'ט Zendesk Chat </p>
            <p class="card-text">מחירים נוחים!</p>
            <p class="card-text">
                <img src="./../assets/img/wix.png"/>
                <img src="./../assets/img/getsocial.png"/>
                <img src="./../assets/img/getresponse.png"/>
                <img src="./../assets/img/zendesk.png"/>
            </p>
        </div>
    </div>
);

