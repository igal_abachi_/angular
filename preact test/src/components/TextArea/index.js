import {h, Component} from 'preact';

export default ({value, name, children, onChange}) => (
    <div class="forn-group">
        <label for="{name}">{children}</label>
        <textarea type="text" class="form-control" id={name} value={value}
                  onChange={e => onChange(e.target.value, name)} rows="5"/>
    </div>
);

