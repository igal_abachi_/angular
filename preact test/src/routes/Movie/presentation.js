import {h, Component} from 'preact';
import {Link} from 'preact-router';

import Input from '../../components/Input'
import TextArea from '../../components/TextArea'

export default ({movie, onChange, onSave}) => (
    <form>
        <Input value={movie.title} name="title" onChange={onChange}>
            Title
        </Input>
        <TextArea value={movie.overview} name="overview" onChange={onChange}>
            Overview
        </TextArea>
        <div class="btn-group">
            <button type="button" class="btn btn-primary" onClick={onSave}>
                Save changes
            </button>
            <Link class="btn btn-warning" href="/movies">
                Cancel
            </Link>
        </div>
    </form>
);

