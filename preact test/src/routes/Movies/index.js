import {h, Component} from 'preact';
import Presentation from "./presentation";

export default class Movies extends Component {
    state = {
        movies: []
    };

    constructor() {
        super();
        this.observer = new IntersectionObserver(this.onIntersection);
        this.observer.POLL_INTERVAL = 100;
    }

    onIntersection = entries => {
        const {movies} = this.state;
        entries.forEach(entry => {
            const id = +entry.target.dataset.id;
            const movie = movies.find(m => m.id === id);
            if (movie) {
                movie.isVisible = movie.isVisible || entry.isIntersecting;
            }
        });
        this.setState({movies})
    };

    componentDidMount() {
        fetch("/api/movies.json")
            .then(rsp => rsp.json())
            .then(movies => this.setState({movies}));
    }

    render({}, {movies}) {
        return (
            <Presentation movies={movies} observer={this.observer}/>
        );
    }
}
