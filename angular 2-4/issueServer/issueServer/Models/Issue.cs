﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace issueServer.Models
{
    public class Issue
    {
        public int id;
        public string title;
        public string text;
        public string[] tags;
    }
}
