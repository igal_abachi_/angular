﻿using issueServer.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace issueServer.Db
{
    public static class DbMock
    {
        public static void Init() { }

        static DbMock()
        {
            LoadFromFile();
        }

        public static int lastIssueId = 0;
        public static int lastCommentId = 0;

        public static readonly Dictionary<int, Issue> Issues = new Dictionary<int, Issue>(40);
        public static readonly Dictionary<int, List<Comment>> Comments = new Dictionary<int, List<Comment>>(40);

        public static void Reset()
        {
            Issues.Clear();
            Comments.Clear();
            lastIssueId = 0;
            lastCommentId = 0;
            LoadFromFile();
        }

        private static void LoadFromFile()
        {
            string json = File.ReadAllText(@".\Db\default_Data.json");
            JObject defulatData = JsonConvert.DeserializeObject(json) as JObject;

            JArray issues = defulatData.GetValue("issues") as JArray;
            for (int i = 0; i < issues.Count; i++)
            {
                Issues[i] = ConvertIssue(issues[i] as JObject);
                lastIssueId++;
            }

            JArray comments = defulatData.GetValue("comments") as JArray;
            for (int i = 0; i < comments.Count; i++)
            {
                var list = new List<Comment>(2);
                Comments[i] = list;

                JArray arr = comments[i] as JArray;
                for (int j = 0; j < arr.Count; j++)
                {
                    list.Add(ConvertComment(arr[j] as JObject));
                    lastCommentId++;
                }
            }
        }

        private static Issue ConvertIssue(JObject issue)
        {
            return new Issue
            {
                id = issue.GetValue("id").Value<int>(),
                title = issue.GetValue("title").Value<string>(),
                text = issue.GetValue("text").Value<string>(),
                tags = issue.GetValue("tags").Values().ToArray().Select(v => v.Value<string>()).ToArray()
            };
        }

        private static Comment ConvertComment(JObject comment)
        {
            return new Comment
            {
                id = comment.GetValue("id").Value<int>(),
                text = comment.GetValue("text").Value<string>()
            };
        }
    }
}
