﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using issueServer.Db;
using issueServer.Models;
using Microsoft.AspNetCore.Cors;

namespace issueServer.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAllOrigins")]
    public class IssuesController : Controller
    {
        // GET api/Issues
        [HttpGet]
        public Issue[] Get()
        {
            var issues = DbMock.Issues.Values.ToArray();
            return issues;
        }

        // GET api/Issues/5
        [HttpGet("{id}")]
        public Issue Get(int id)
        {
            Issue issue;
            if (DbMock.Issues.TryGetValue(id, out issue))
                return issue;
            return null;
        }

        // POST api/Issues
        [HttpPost]
        public int Post([FromBody]Issue value)
        {
            value.id = DbMock.lastIssueId++;
            DbMock.Issues[value.id] = value;
            return value.id;
        }

        // PUT api/Issues/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Issue value)
        {
            DbMock.Issues[id] = value;
        }

    }
}
