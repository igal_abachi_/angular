﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using issueServer.Db;
using issueServer.Models;
using Microsoft.AspNetCore.Cors;

namespace issueServer.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAllOrigins")]
    public class CommentsController : Controller
    {
        // GET api/comments
        [HttpGet("{id}")]
        public Comment[] Get(int id)// issue id
        {
            List<Comment> comments;
            if (DbMock.Comments.TryGetValue(id, out comments))
                return comments.ToArray();
            return new Comment[0];
        }

        // POST api/comments/5
        [HttpPost("{id}")]
        public int Post(int id, [FromBody]Comment value)
        {
            value.id = DbMock.lastCommentId++;
            List<Comment> comments;
            if (!DbMock.Comments.TryGetValue(id, out comments))
            {
                comments = new List<Comment>();
                DbMock.Comments[id] = comments;
            }
            comments.Add(value);

            return value.id;
        }

        // PUT api/comments/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Comment value)
        {
            List<Comment> comments;
            if (DbMock.Comments.TryGetValue(id, out comments))
            {
                for (int i = 0; i < comments.Count; i++)
                {
                    var c = comments[i];
                    if (c.id == value.id)
                    {
                        c.text = value.text;
                        break;
                    }
                }
            }
        }

        // DELETE api/comments/5
        [HttpDelete("{id}/{cId}")]
        public void Delete(int id, int cId)
        {
            List<Comment> comments;
            if (DbMock.Comments.TryGetValue(id, out comments))
            {
                for (int i = 0; i < comments.Count; i++)
                {
                    var c = comments[i];
                    if (c.id == cId)
                    {
                        comments.RemoveAt(i);
                        break;
                    }
                }
            }
        }
    }
}
