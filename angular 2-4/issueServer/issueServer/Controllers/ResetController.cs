﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using issueServer.Db;
using issueServer.Models;
using Microsoft.AspNetCore.Cors;

namespace issueServer.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowAllOrigins")]
    public class ResetController : Controller
    {
        // GET api/reset
        [HttpGet]
        public string Get()
        {
            DbMock.Reset();
            return "DbMock was reset";
        }
    }
}
