import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {Issue} from "../../Models/Issue.model";
import {Tag} from "../../Models/tag.model";
import {UiData} from "../../Services/UI-Data.service";
import {Router} from "@angular/router";

@Component({
  selector: 'issues-list-item',
  templateUrl: './issues-list-item.component.html',
  styleUrls: ['./issues-list-item.component.scss']
})
export class IssuesListItemComponent implements OnInit {

  @Input() issue: Issue = new Issue();
  tags: Tag[] = [];

  constructor(private uiData: UiData, private router: Router) {
  }

  ngOnInit() {
    let t = this.issue.tags;
    this.tags.splice(0,this.tags.length);
    for (let i = 0; i < t.length; i++) {
      this.tags.push(this.uiData.tags[t[i]]);
    }

  }

  gotoIssue() {
    console.log("goto Issue: " + this.issue.id);
    this.router.navigate(["/Issue", this.issue.id]);
  }

}
