import {AfterViewInit, Component, ElementRef, Input, ViewChild} from '@angular/core';
import {CommentModel} from "../../Models/comment.model";
import {Issue} from "../../Models/Issue.model";
import {CommentsApi} from "../../Services/CommentsApi.service";
import {IssuesApi} from "../../Services/IssuesApi.service";
import {CommentEditorComponent} from "../comment-editor/comment-editor.component";

@Component({
  selector: 'comment-view',
  templateUrl: './comment-view.component.html',
  styleUrls: ['./comment-view.component.scss']
})
export class CommentViewComponent implements AfterViewInit {

  @Input() issueId: number = -1;
  @Input() isNew: boolean = false;
  @Input() comment: CommentModel = new CommentModel();
  @Input() issue: Issue = new Issue();
  @Input() isIssue: boolean = false;
  @Input() isInEditMode: boolean = false;


  htmlBeforeEdit: string = "";

  @ViewChild('cView') cView: ElementRef;

  @ViewChild('issueEdit') issueEdit: CommentEditorComponent;
  @ViewChild('commentEdit') commentEdit: CommentEditorComponent;


  constructor(private commentsApi: CommentsApi, private issuesApi: IssuesApi) {
    this.issue.text = "";
  }

  ngAfterViewInit() {
    this.refresh();
    setTimeout(() => {
      this.refresh();
    }, 1000);
  }

  refresh() {
    if (!this.isIssue) {
      this.updateView(this.comment.text);
    }
    else {
      this.updateView(this.issue.text);
    }
  }

  updateView(html: string) {

    // match & replace math expressions with answer in html , before setting innerHTML: "2+1*4-3"
    // let regexMath = /^([-+/*]\d+(\.\d+)?)*/;
    // let mathExpArr = html.match(regexMath);
    // if (mathExpArr != null)
    //   for (let i = 0; i < mathExpArr.length; i++) {
    //     let matchedStr = mathExpArr[i];
    //     if (matchedStr == null || matchedStr.length === 0) continue;
    //
    //     html.replace(matchedStr, eval(matchedStr).toString());
    //   }


    this.cView.nativeElement.innerHTML = html;
  }

  backup() {
    this.htmlBeforeEdit = !this.isIssue ? this.comment.text : this.issue.text;

    this.issueEdit.setHtml(this.issue.text);
    this.commentEdit.setHtml(this.comment.text);
  }


  onCancel() {
    if (!this.isIssue) {
      this.comment.text = this.htmlBeforeEdit;
    }
    else {
      this.issue.text = this.htmlBeforeEdit;
    }

    this.isInEditMode = false;
    this.refresh();
  }

  onSave() {
    this.isInEditMode = false;
    if (!this.isIssue) {
      this.comment.text = this.commentEdit.getNewHtml();
      this.commentsApi.updateComment(this.comment, this.issueId).subscribe(
        v => {
        },
        error => {
          console.log(<any>error);
          this.reloadPage();
        });
    }
    else {
      this.issue.text = this.issueEdit.getNewHtml();
      this.issuesApi.updateIssue(this.issue).subscribe(
        v => {
        },
        error => {
          console.log(<any>error);
          this.reloadPage();
        });
    }
  }

  forceNewCommentData() {
    this.comment.text = this.commentEdit.getNewHtml();
  }

  onDelete() {
    this.isInEditMode = false;
    this.commentsApi.deleteComment(this.issueId, this.comment.id).subscribe(
      v => {
        this.reloadPage();
      },
      error => {
        console.log(<any>error);
        this.reloadPage();
      });
  }

  reloadPage() {
    window.location.href = (window.location.origin + "/Issue/" + (this.isIssue ? this.issue.id : this.issueId));
  }
}
