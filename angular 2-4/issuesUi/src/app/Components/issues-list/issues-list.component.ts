import {Component, Input, OnInit} from '@angular/core';
import {Issue} from "../../Models/Issue.model";

@Component({
  selector: 'issues-list',
  templateUrl: './issues-list.component.html',
  styleUrls: ['./issues-list.component.scss']
})
export class IssuesListComponent implements OnInit {

  @Input() issues: Issue[] = [];

  constructor() {
  }

  ngOnInit() {
  }

}
