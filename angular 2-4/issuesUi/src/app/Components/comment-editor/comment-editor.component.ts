import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {UiData} from "../../Services/UI-Data.service";

@Component({
  selector: 'comment-editor',
  templateUrl: './comment-editor.component.html',
  styleUrls: ['./comment-editor.component.scss']
})
export class CommentEditorComponent implements OnInit {


  @Input() htmlContent: string = "<span style='font-size: 12pt;'> text </span>";
  @ViewChild('ta') ta: ElementRef;

  constructor(private uiData: UiData) {
  }

  ngOnInit() {
  }

  public setHtml(html: string) {// restore value, no [(ngModel)] 2 way binding for <textArea/>
    this.htmlContent = html;
    return this.ta.nativeElement.value = this.htmlContent;
  }

  public getNewHtml(): string {
    return this.ta.nativeElement.value;
  }

  htmlChanged(e) {
    this.htmlContent = e.target.value;
  }


  doFormat(type: string, param: string = "") {
    let frmt = this.uiData.formatting[type];
    let editor = this.ta.nativeElement;

    let s = editor.selectionStart;
    let e = editor.selectionEnd;

    if (type === "font") {
      let font = param;

      let isSafeFont = (font === "Arial" ||
        font === "Courier New" ||
        font === "Tahoma" ||
        font === "Times New Roman" ||
        font === "Verdana");

      if (!isSafeFont) {
        font += ', "Helvetica Neue", Helvetica, Arial, sans-serif';
      }
      frmt = frmt.replace("===", font);
    }
    else if (type === "color") {
      frmt = frmt.replace("===", param);
    }

    let surround = Math.max(0, e - s) > 1;
    let newHtml = this.htmlContent;
    //
    let tillIdx = s + 1;
    let fmtEval = " ";
    let fromIdx = tillIdx;
    if (surround) {
      tillIdx = s;
      fmtEval = this.htmlContent.substring(s * 1, e);
      fromIdx = e;
    }
    newHtml = this.htmlContent.substring(0, tillIdx);
    newHtml += frmt.replace("#~#", fmtEval);
    newHtml += this.htmlContent.substring(fromIdx);

    this.setHtml(newHtml);
  }
}
