import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Issue} from "../../Models/Issue.model";
import {UiData} from "../../Services/UI-Data.service";
import {Tag} from "../../Models/tag.model";

@Component({
  selector: 'issues-filter',
  templateUrl: './issues-filter.component.html',
  styleUrls: ['./issues-filter.component.scss']
})
export class IssuesFilterComponent implements OnInit {

  allTags: Tag[] = [];

  constructor(private uiData: UiData) {
    this.allTags = (<any>Object).values(uiData.tags);
  }

  @Input() issues: Issue[] = [];
  @Output() onFilter = new EventEmitter();

  selectedTag: string = "";
  filteredIssues: Issue[] = [];


  ngOnInit() {
    this.reset();
  }

  reset() {
    this.selectedTag = "";
    for (let i = 0; i < this.issues.length; i++) {
      let issue = this.issues[i];
      this.filteredIssues.push(issue);
    }
    this.onFilter.emit(this.filteredIssues);
  }

  onTagChange(e) {
    this.selectedTag = e;
    this.filteredIssues.splice(0, this.filteredIssues.length);

    for (let i = 0; i < this.issues.length; i++) {
      let issue = this.issues[i];
      if (this.selectedTag === "" || issue.tags.indexOf(this.selectedTag) > -1) {
        this.filteredIssues.push(issue);
      }
    }
    this.onFilter.emit(this.filteredIssues);
  }

}
