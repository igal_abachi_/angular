
import { Injectable } from '@angular/core';

@Injectable()
export class Utils {
  // min & max inclusive
  public getRandomInt( min: number, max: number ): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    let r = Math.floor(Math.random() * (max - min + 1)) + min;
    return r;
  }

  public getRandomItem( arr: any[] ): any {
    return arr[this.getRandomInt(0, (arr.length - 1))];
  }

}
/*
export class Converter<T> {

  public ConvertArrayType( arr: any[], field: string ): T[] {
    return <T[]>arr.map(obj => {
      let newObj: T;
      Object.assign(newObj, obj[field]);
      return newObj;
    });
  }

  public Extend( target: T, src: any ): T {
    Object.assign(target, src);
    return <T>target;
  }

  public Convert( src: any ): T {
    try {
      let target: T;
      Object.assign(target, src);
      return <T>target;
    }
    catch (err) {
      console.error(err);
      return <T>src;
  }
  }
}

*/


