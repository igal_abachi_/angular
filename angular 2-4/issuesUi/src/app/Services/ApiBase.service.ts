
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { Http, Response, RequestOptions, Headers } from "@angular/http";

export abstract class ApiBase {
  private url: string = "http://localhost:3000/api/";

  public GetServerUrl(): string {
    return this.url;
  }


  private http: Http;

  constructor( httpRequest: Http ) {
    this.http = httpRequest;
  }

  private extractData( res: Response ) {
    let body = res.json();
    body = body || {};
    if (body.errorCode_ == 404) {
      console.error(body);
    }
    return body;
  }


  private handleError( error: any ) {
    let errMsg = (error.message) ? error.message :
                 error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private GetHeaders() {
    return new Headers({'Accept': 'application/json'});// application/x-msgpack
}

  private PostHeaders() {
    return new Headers({'Content-Type': 'application/json'});
  }

  GETLocalFile( path: string ): Observable<string> {
    console.log("GET: " + path);
    let addr: string = "./" + path;
    return this.http.get(addr, this.GetHeaders())
               .map(res => res.text())
               .catch(this.handleError);
  }

  GET<T>( method: string, param: string ): Observable<T> {
    console.log("GET: " + method + "(" + param + ")");
    let addr: string = this.url + method + (param == null ? "" : "/" + param);
    return this.http.get(addr, this.GetHeaders())
               .map(this.extractData)
               .catch(this.handleError);
  }

  DELETE<T>( method: string, param: string ): Observable<T> {
    console.log("DELETE: " + method + "(" + param + ")");
    let addr: string = this.url + method + (param == null ? "" : "/" + param);
    return this.http.delete(addr, this.GetHeaders())
      .map(this.extractData)
      .catch(this.handleError);
  }

  GET0Q<T>( method: string, queryStr: string ): Observable<T> {
    console.log("GET: " + method + "( ?" + queryStr + ")");
    let addr: string = this.url + method + "?" + queryStr ;
    return this.http.get(addr, this.GetHeaders())
               .map(this.extractData)
               .catch(this.handleError);
  }

  GET1Q<T>( method: string, param: string, queryStr: string ): Observable<T> {
    console.log("GET: " + method + "(" + param + " , ?" + queryStr + ")");
    let addr: string = this.url + method + "/" + param + "?" + queryStr ;
    return this.http.get(addr, this.GetHeaders())
               .map(this.extractData)
               .catch(this.handleError);
  }

  GET2<T>( method: string, param: string, param2: string ): Observable<T> {
    console.log("GET: " + method + "(" + param + "," + param2 + ")");
    let addr: string = this.url + method + "/" + param + "/" + param2;

    return this.http.get(addr, this.GetHeaders())
               .map(this.extractData)
               .catch(this.handleError);
  }

  GET3<T>( method: string, param: string, param2: string, param3: string ): Observable<T> {
    console.log("GET: " + method + "(" + param + "," + param2 + "," + param3 + ")");
    let addr: string = this.url + method + "/" + param + "/" + param2 + "/" + param3;

    return this.http.get(addr, this.GetHeaders())
               .map(this.extractData)
               .catch(this.handleError);
  }

  GET2Q<T>( method: string, param: string, param2: string, queryStr ): Observable<T> {
    console.log("GET: " + method + "(" + param + "," + param2 + " , ?" + queryStr + ")");
    let addr: string = this.url + method + "/" + param + "/" + param2 + "?" + queryStr ;
    return this.http.get(addr, this.GetHeaders())
               .map(this.extractData)
               .catch(this.handleError);
  }

  POST<T>( method: string, obj: any ): Observable<T> {
    let body = JSON.stringify(obj);
    console.log("POST: " + method + "(" + body + ")");

    let options = new RequestOptions({headers: this.PostHeaders()});
    return this.http.post(this.url + method, body, options)
               .map(this.extractData)
               .catch(this.handleError);
  }

  PUT<T>( method: string, obj: any ): Observable<T> {
    let body = JSON.stringify(obj);
    console.log("PUT: " + method + "(" + body + ")");

    let options = new RequestOptions({headers: this.PostHeaders()});
    return this.http.put(this.url + method, body, options)
      .map(this.extractData)
      .catch(this.handleError);
  }
}
