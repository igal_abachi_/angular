import {Injectable} from '@angular/core';

import {Observable} from "rxjs";
import {ApiBase} from "./ApiBase.service";
import {Http} from "@angular/http";
import {CommentModel} from "../Models/comment.model";

@Injectable()
export class CommentsApi extends ApiBase {
  constructor(http: Http) {
    super(http);
  }

  getComments(issueId: number): Observable<CommentModel[]> {
    return super.GET<CommentModel[]>("comments", issueId.toString());
  }

  createComment(comment: CommentModel, issueId: number): Observable<any> {
    return super.POST<void>("comments/" + issueId, comment);
  }

  updateComment(comment: CommentModel, issueId: number): Observable<any> {
    return super.PUT<void>("comments/" + issueId, comment);
  }

  deleteComment(issueId: number, commentId: number,): Observable<any> {
    return super.DELETE<void>("comments/" + issueId + "/", commentId.toString());
  }
}
