import {Injectable} from '@angular/core';

import {Observable} from "rxjs";
import {ApiBase} from "./ApiBase.service";
import {Http} from "@angular/http";
import {Issue} from "../Models/Issue.model";

@Injectable()
export class IssuesApi extends ApiBase {
  constructor(http: Http) {
    super(http);
  }

  getAllIssues(): Observable<Issue[]> {
    return super.GET<Issue[]>("issues", null);
  }

  getIssue(id: number): Observable<Issue> {
    return super.GET<Issue>("issues", id.toString());
  }

  createIssue(issue: Issue): Observable<any> {
    return super.POST<void>("issues", issue);
  }

  updateIssue(issue: Issue): Observable<any> {
    return super.PUT<void>("issues/" + issue.id, issue);
  }
}
