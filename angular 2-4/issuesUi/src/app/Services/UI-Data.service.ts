import {Injectable} from '@angular/core';
import {Tag} from "../Models/tag.model";

@Injectable()
export class UiData {
  tags: any = {
    "bug": new Tag("bug", "#6f0000", false),
    "enhancement": new Tag("enhancement", "#10607f", false),
    "fixed": new Tag("fixed", "#317816", false),
    "issue": new Tag("issue", "#9f162a", false),
    "proposal": new Tag("proposal", "#afa42f", false),
    "need-repro": new Tag("need-repro", "#8f5425", false)
  };

  formatting: any = {
    "bold": "\n<span style='font-weight: bold;'>#~#</span>\n",
    "italic": "\n<span style='font-style: italic;'>#~#</span>\n",
    "underline": "\n<span style='text-decoration: underline;'>#~#</span>\n",
    "large": "\n<span style='font-size: 18pt;'>#~#</span>\n",
    "small": "\n<span style='font-size: 9pt;'>#~#</span>\n",
    "color": "\n<span style='color: ===;'>#~#</span>\n",
    "font": "\n<span style='font-family: ===;'>#~#</span>\n",
  };

}
