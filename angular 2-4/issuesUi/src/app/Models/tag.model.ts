export class Tag {
  constructor(name: string, clr: string, isUserDefinedTag = true) {
    this.tagName = name;
    this.color = clr;
    this.isUserDefinedTag = isUserDefinedTag;
  }

  tagName: string;
  color: string = "#00507f";
  isChecked: boolean = false;
  isUserDefinedTag: boolean = true;
}

