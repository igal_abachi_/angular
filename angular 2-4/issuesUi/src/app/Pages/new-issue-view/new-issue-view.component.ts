import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Issue} from "../../Models/Issue.model";
import {Router} from "@angular/router";
import {IssuesApi} from "../../Services/IssuesApi.service";
import {CommentEditorComponent} from "../../Components/comment-editor/comment-editor.component";

@Component({
  selector: 'new-issue-view',
  templateUrl: './new-issue-view.component.html',
  styleUrls: ['./new-issue-view.component.scss']
})
export class NewIssueViewComponent implements AfterViewInit {
  issue: Issue = new Issue();
  @ViewChild('titleTxt') titleTxt: ElementRef;
  @ViewChild('issueEdit') issueEdit: CommentEditorComponent;

  constructor(private router: Router, private issuesApi: IssuesApi) {
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.titleTxt.nativeElement.focus();
    }, 600);
  }

  onSave() {
    this.issue.text = this.issueEdit.getNewHtml();
    if (this.issue.title == null || this.issue.title.length === 0)
      this.issue.title = "no title";

    this.issuesApi.createIssue(this.issue).subscribe(
      newIssueId => {
        this.router.navigate(["/Issue", newIssueId]);
      },
      error => {
        console.log(<any>error);
        this.router.navigate(["/Issues"]);
      });
  }

  onCancel() {

    this.router.navigate(["/Issues"]);
  }

}
