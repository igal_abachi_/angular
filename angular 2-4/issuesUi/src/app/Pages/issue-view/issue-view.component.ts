import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Issue} from "../../Models/Issue.model";
import {CommentModel} from "../../Models/comment.model";
import {ActivatedRoute, Router} from "@angular/router";
import {UiData} from "../../Services/UI-Data.service";
import {Tag} from "../../Models/tag.model";
import {IssuesApi} from "../../Services/IssuesApi.service";
import {CommentsApi} from "../../Services/CommentsApi.service";
import {CommentViewComponent} from "../../Components/comment-view/comment-view.component";

@Component({
  selector: 'issue-view',
  templateUrl: './issue-view.component.html',
  styleUrls: ['./issue-view.component.scss']
})
export class IssueViewComponent implements AfterViewInit {
  issueId: number = -1;
  issue: Issue = new Issue();
  issueComment: CommentModel = new CommentModel();
  comments: CommentModel[] = [];
  newComment: CommentModel = new CommentModel();

  @ViewChild('newCommentEdit') newCommentEdit: CommentViewComponent;

  allTags: Tag[] = [];

  constructor(private route: ActivatedRoute, private router: Router, private uiData: UiData, private issuesApi: IssuesApi, private commentsApi: CommentsApi) {
    this.issueId = +this.route.snapshot.params['id'];

    this.allTags = (<any>Object).values(uiData.tags);

    for (let i = 0; i < this.allTags.length; i++) {
      this.allTags[i].isChecked = false;
    }
    this.issue.text = "";

    this.issuesApi.getIssue(this.issueId).subscribe(
      issue => {
        this.issue = issue;

        for (let i = 0; i < this.issue.tags.length; i++) {
          let t = this.issue.tags[i];

          for (let j = 0; j < this.allTags.length; j++) {
            let tag = this.allTags[j];
            if (tag.tagName === t) {
              tag.isChecked = true;
            }
          }
        }
      },
      error => console.log(<any>error));


    this.commentsApi.getComments(this.issueId).subscribe(
      comments => {
        this.comments = comments;
      },
      error => console.log(<any>error));

  }

  ngAfterViewInit() {
  }

  onSave() {
    this.issue.tags.splice(0, this.issue.tags.length);
    for (let i = 0; i < this.allTags.length; i++) {
      let t = this.allTags[i];
      if (t.isChecked)
        this.issue.tags.push(t.tagName);
    }

    this.issuesApi.updateIssue(this.issue).subscribe(
      v => {
      },
      error => {
        console.log(<any>error);

        this.newCommentEdit.forceNewCommentData();
        ;
        if (this.newComment.text != null && this.newComment.text.length > 0) {
          this.commentsApi.createComment(this.newComment, this.issueId).subscribe(
            newCommentId => {
              this.goBack();
            },
            error2 => {
              console.log(<any>error2);
              this.goBack();
            });
        }
        else {
          this.goBack();
        }
      });
  }

  onCancel() {
    this.goBack();
  }

  goBack() {
    this.router.navigate(["/Issues"]);
  }
}
