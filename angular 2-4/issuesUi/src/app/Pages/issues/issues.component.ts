import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Issue} from "../../Models/Issue.model";
import {Router} from "@angular/router";
import {Utils} from "../../Services/Utils.service";
import {UiData} from "../../Services/UI-Data.service";
import {IssuesApi} from "../../Services/IssuesApi.service";
import {IssuesFilterComponent} from "../../Components/issues-filter/issues-filter.component";

@Component({
  selector: 'issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.scss']
})
export class IssuesComponent implements OnInit {

  issues: Issue[] = [];
  filtered: Issue[] = [];

  @ViewChild('filterBox') filterBox: IssuesFilterComponent;

  constructor(private router: Router, private utils: Utils, private uiData: UiData, private issuesApi: IssuesApi) {

    this.issuesApi.getAllIssues().subscribe(
      issues => {
        this.issues = issues;
        setTimeout(() => {
          this.filterBox.reset();
        }, 0);
      },
      error => console.log(<any>error));

    /*
    let tags = Object.keys(uiData.tags);
    for (let i = 0; i < 40; i++) {
      let issue = new Issue();
      issue.id = i;
      issue.text = "text";
      issue.title = "issue " + (i + 1);

      let bugCount = Math.max(1, utils.getRandomInt(1, 3) - utils.getRandomInt(0, 1));
      for (let j = 0; j < bugCount; j++)
        issue.tags.push(utils.getRandomItem(tags));

      issue.tags = issue.tags.filter(function (v, i) {
        return issue.tags.indexOf(v) === i;
      });
      this.issues.push(issue);
    }
    */

  }

  ngOnInit() {
  }

  onFilter(issues: Issue[]) {
    this.filtered = issues;
  }

  gotoNewIssue() {

    this.router.navigate(["/NewIssue"]);
  }
}
