import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'log'})
export class Log implements PipeTransform {
  transform( value: any ): any {
    if (value != null)
      console.log(value);

    return value;
  }
}
