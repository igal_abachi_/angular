import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from "@angular/core";

import {IssuesComponent} from "./Pages/issues/issues.component";
import {IssueViewComponent} from "./Pages/issue-view/issue-view.component";
import {NewIssueViewComponent} from "./Pages/new-issue-view/new-issue-view.component";

const appRoutes: Routes = [
  {path: '', redirectTo: '/Issues', pathMatch: 'full'},
  {path: 'Issues', component: IssuesComponent},
  {path: 'NewIssue', component: NewIssueViewComponent},
  {path: 'Issue/:id', component: IssueViewComponent},
  //new tag route page?
  {path: '**', component: IssuesComponent}
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
