import {BrowserModule} from '@angular/platform-browser';

//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {Injectable, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule, Http} from '@angular/http';

import {AppComponent} from './app.component';
import {routing, appRoutingProviders} from './app.routing';


import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

//import { MenuComponent } from './Components/Menu/menu.component';

import {Log} from "./Pipes/Log.pipe";
import {UiData} from "./Services/UI-Data.service";

import {IssuesApi} from './Services/IssuesApi.service';
import {CommentsApi} from './Services/CommentsApi.service';
import {IssuesComponent} from './Pages/issues/issues.component';
import {IssuesFilterComponent} from './Components/issues-filter/issues-filter.component';
import {IssuesListComponent} from './Components/issues-list/issues-list.component';
import {IssuesListItemComponent} from './Components/issues-list-item/issues-list-item.component';
import {IssueViewComponent} from './Pages/issue-view/issue-view.component';
import {NewIssueViewComponent} from './Pages/new-issue-view/new-issue-view.component';
import {CommentViewComponent} from './Components/comment-view/comment-view.component';
import {CommentEditorComponent} from './Components/comment-editor/comment-editor.component';
import {Utils} from "./Services/Utils.service";


@NgModule({
  declarations: [
    AppComponent,
    IssuesComponent,
    Log,
    IssuesFilterComponent,
    IssuesListComponent,
    IssuesListItemComponent,
    IssueViewComponent,
    NewIssueViewComponent,
    CommentViewComponent,
    CommentEditorComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    routing,
  ],
  providers: [IssuesApi, CommentsApi, UiData, Utils, appRoutingProviders /*, BrowserAnimationsModule*/],
  bootstrap: [AppComponent]
})
export class AppModule {
}
