import { ViventiumPage } from './app.po';

describe('viventium App', function() {
  let page: ViventiumPage;

  beforeEach(() => {
    page = new ViventiumPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
